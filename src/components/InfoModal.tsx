import React, { FunctionComponent } from "react";

const InfoModal: FunctionComponent<any> = ({
  info,
  setInfo,
  showInfo,
  movie,
}): any => {
  let imageBaseUri: string = "http://image.tmdb.org/t/p/";
  let imageSizehigh: string = "/w1280/";
  let imageSizeOriginal: string = "/original";

  const styles = {
    infoModal: {
      backgroundImage: `url(${imageBaseUri + imageSizehigh + movie.backdrop})`,
    } as React.CSSProperties,
    info: {
      backgroundImage: `url(${
        imageBaseUri + imageSizeOriginal + movie.poster
      } )`,
    },
  };

  showInfo = () => {
    setInfo(!info);
  };

  return (
    <>
      {movie && (
        <>
          <div className="infoModal" style={styles.infoModal}>
            <span className="closeBtn" onClick={showInfo}>
              &#x2715;
            </span>
            <div className="infoContent poster">
              <img
                src={imageBaseUri + imageSizeOriginal + movie.poster}
                alt=""
              />
            </div>
            <div className="infoDetails">
              <h1>{movie.title}</h1>
              <p>{movie.overview}</p>
            </div>
          </div>
        </>
      )}
    </>
  );
};

export default InfoModal;
