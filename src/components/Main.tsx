import React, { FunctionComponent, useEffect, useState } from "react";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { ThunkDispatch } from "redux-thunk";
import { AppState } from "../store/rootStore";
import {} from "../store/movies/models/data.type";
import { Movie } from "../store/movies/models/data.type";
import {
  fetchNowPlaying,
  fetchPopular,
  fetchUpcoming,
} from "../store/movies/moviesAction";
import { AppActions } from "../store/models/actions";
import MovieCard from "./MovieCard";
import InfoModal from "./InfoModal";

export interface StateProps {
  loading: boolean;
  popular: Movie[];
  playing: Movie[];
  upcoming: Movie[];
  error: string;
}

interface ActionProps {
  fetchNowPlaying: () => void;
  fetchPopular: () => void;
  fetchUpcoming: () => void;
}

type ReduxProps = StateProps & ActionProps;

const mapStatetoProps = (state: AppState): StateProps => ({
  popular: state.moviesReducer.popular,
  playing: state.moviesReducer.playing,
  upcoming: state.moviesReducer.upcoming,
  loading: state.moviesReducer.loading,
  error: state.moviesReducer.error,
});

const mapDispatchToProps = (
  dispatch: ThunkDispatch<AppState, { movie: Movie }, AppActions>
): ActionProps => ({
  fetchNowPlaying: bindActionCreators(fetchNowPlaying, dispatch),
  fetchPopular: bindActionCreators(fetchPopular, dispatch),
  fetchUpcoming: bindActionCreators(fetchUpcoming, dispatch),
});

const Main: FunctionComponent<ReduxProps> = (props) => {
  const {
    error,
    loading,
    playing,
    popular,
    upcoming,
    fetchNowPlaying,
    fetchPopular,
    fetchUpcoming,
  } = props;
  let imageLink: string = "http://image.tmdb.org/t/p/original";

  const [info, setInfo] = useState(false);


  useEffect(() => {
    fetchNowPlaying();
    fetchPopular();
    fetchUpcoming();
    console.log(playing);
  }, []);

  return (
    <div className="wrapper">
      <div className="category">
        <h2>Now Playing Movies</h2>
        {loading && <p>Loading...</p>}

        <div className="container">
          {playing.slice(0, 5).map((movie: Movie) => (
            <MovieCard movie={movie} key={movie.id} />
          ))}
        </div>
        {error && <div>{error}</div>}
      </div>
      <div className="category">
        <h2>Popular Movies</h2>
        {loading && <p>Loading...</p>}

        <div className="container">
          {popular.slice(0, 5).map((movie: Movie) => (
            <MovieCard movie={movie} key={movie.id} />
          ))}
        </div>
        {error && <div>{error}</div>}
      </div>
      <div className="category">
        <h2>Upcoming Movies</h2>
        {loading && <p>Loading...</p>}

        <div className="container">
          {upcoming.slice(0, 5).map((movie: Movie) => (
            <MovieCard movie={movie} key={movie.id} />
          ))}
        </div>
        {error && <div>{error}</div>}
      </div>
    </div>
  );
};

// class Main extends Component<Props> {
//   componentDidMount() {
//     this.props.fetchNowPlaying();
//   }
//   render() {
//     return <div>Movies List</div>;
//   }
// }

export default connect(mapStatetoProps, mapDispatchToProps)(Main);
