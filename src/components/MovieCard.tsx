import React, { useState, useEffect, FunctionComponent } from "react";
import InfoModal from "./InfoModal";
import { Movie } from "../store/movies/models/data.type";

const MovieCard: FunctionComponent<any> = ({ movie }) => {
  let imageLink: string = "http://image.tmdb.org/t/p/w500";
  const [info, setInfo] = useState(false);

  const { poster, backdrop, title, release_date } = movie;

  const showInfo = () => {
    setInfo(!info);
  };

  useEffect(() => {
    console.log(info);
  }, [info, showInfo]);

  return (
    <>
      <div className="cardWrapper" onClick={showInfo}>
        <div className="cardImage">
          <img src={imageLink + poster} alt={"Movie poster for " + title} />
        </div>
        <div className="movieDetails">
          <p>{title}</p>
          <p>{release_date}</p>
        </div>
      </div>
      {info && <InfoModal movie={movie} info={info} setInfo={setInfo} showInfo={showInfo} />}
    </>
  );
};

export default MovieCard;
