import { Movie } from "./data.type";

interface bareData {
  loading: boolean;
  error: string;
}

interface Popular extends bareData {
  popular: Movie[];
}

interface Playing extends bareData {
  playing: Movie[];
}

interface Upcoming extends bareData {
  upcoming: Movie[];
}

export const REQUEST_NOWPLAYING = "REQUEST_NOWPLAYING";
export const NOWPLAYING_LOADED = "NOWPLAYING_LOADED";
export const NOWPLAYING_ERROR = "NOWPLAYING_ERROR";

export const REQUEST_POPULAR = "REQUEST_POPULAR";
export const POPULAR_LOADED = "POPULAR_LOADED";
export const POPULAR_ERROR = "POPULAR_ERROR";

export const REQUEST_UPCOMING = "REQUEST_UPCOMING";
export const UPCOMING_LOADED = "UPCOMING_LOADED";
export const UPCOMING_ERROR = "UPCOMING_ERROR";

interface requestNowPlaying extends Playing {
  type: typeof REQUEST_NOWPLAYING;
}

interface nowPlayingLoaded extends Playing {
  type: typeof NOWPLAYING_LOADED;
}

interface nowPlayingError extends Playing {
  type: typeof NOWPLAYING_ERROR;
}

interface requestPopular extends Popular {
  type: typeof REQUEST_POPULAR;
}

interface popularLoaded extends Popular {
  type: typeof POPULAR_LOADED;
}

interface popularError extends Popular {
  type: typeof POPULAR_ERROR;
}

interface requestUpcoming extends Upcoming {
  type: typeof REQUEST_UPCOMING;
}

interface upcomingLoaded extends Upcoming {
  type: typeof UPCOMING_LOADED;
}

interface upcomingError extends Upcoming {
  type: typeof UPCOMING_ERROR;
}

export type moviesActionsTypes =
  | requestNowPlaying
  | nowPlayingLoaded
  | nowPlayingError
  | requestPopular
  | popularLoaded
  | popularError
  | requestUpcoming
  | upcomingLoaded
  | upcomingError;
