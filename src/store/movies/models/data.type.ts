export interface Movie {
  id: number;
  title: string;
  overview: string;
  poster: string;
  backdrop: string;
  popularity: number;
  release_date: string;
}
