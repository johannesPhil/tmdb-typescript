import {
  moviesActionsTypes,
  REQUEST_NOWPLAYING,
  REQUEST_POPULAR,
  REQUEST_UPCOMING,
  POPULAR_LOADED,
  NOWPLAYING_LOADED,
  UPCOMING_LOADED,
  NOWPLAYING_ERROR,
  POPULAR_ERROR,
  UPCOMING_ERROR,
} from "./models/actions.types";
import { Movie } from "./models/data.type";

interface bareState {
  loading: boolean;
  error: string;
  popular: Movie[];
  playing: Movie[];
  upcoming: Movie[];
}

interface popularState extends bareState {
  popular: Movie[];
}

interface playingState extends bareState {
  playing: Movie[];
}

interface upcomingState extends bareState {
  upcoming: Movie[];
}

export type moviesState = popularState | playingState | upcomingState;

// interface moviesState {
//   loading: boolean;
//   popular: Movie[];
//   playing: Movie[];
//   upcoming: Movie[];
//   error: string;
// }

const initState: moviesState = {
  loading: false,
  error: "",
  popular: [],
  playing: [],
  upcoming: [],
};

export const moviesReducer = (
  state = initState,
  action: moviesActionsTypes
): bareState => {
  switch (action.type) {
    case REQUEST_NOWPLAYING:
      return {
        ...state,
        loading: action.loading,
        playing: [],
        error: action.error,
      };

    case REQUEST_POPULAR:
      return {
        ...state,
        loading: action.loading,
        popular: [],
        error: action.error,
      };

    case REQUEST_UPCOMING:
      return {
        ...state,
        loading: action.loading,
        upcoming: [],
        error: action.error,
      };

    case NOWPLAYING_LOADED:
      return {
        ...state,
        loading: action.loading,
        playing: action.playing,
        error: action.error,
      };

    case POPULAR_LOADED:
      return {
        ...state,
        loading: action.loading,
        popular: action.popular,
        error: action.error,
      };

    case UPCOMING_LOADED:
      return {
        ...state,
        loading: action.loading,
        upcoming: action.upcoming,
        error: action.error,
      };

    case NOWPLAYING_ERROR:
      return {
        ...state,
        loading: action.loading,
        playing: [],
        error: action.error,
      };

    case POPULAR_ERROR:
      return {
        ...state,
        loading: action.loading,
        popular: [],
        error: action.error,
      };

    case UPCOMING_ERROR:
      return {
        ...state,
        loading: action.loading,
        upcoming: [],
        error: action.error,
      };

    default:
      return state;
  }
};
