import { Dispatch } from "redux";

import {
  REQUEST_NOWPLAYING,
  REQUEST_POPULAR,
  REQUEST_UPCOMING,
  POPULAR_LOADED,
  NOWPLAYING_LOADED,
  UPCOMING_LOADED,
  NOWPLAYING_ERROR,
  POPULAR_ERROR,
  UPCOMING_ERROR,
} from "./models/actions.types";
import { AppActions } from "../models/actions";
import { Movie } from "./models/data.type";
import axios from "axios";

const apikey: string = process.env.REACT_APP_API_KEY as string;
const nowPlayingLink = `https://api.themoviedb.org/3/movie/now_playing?api_key=${apikey}&language=en-US`;
const popularLink = `https://api.themoviedb.org/3/movie/popular?api_key=${apikey}&language=en-US`;
const upcomingLink = `https://api.themoviedb.org/3/movie/upcoming?api_key=${apikey}&language=en-US`;

interface objectCell extends Record<string, any> {
  id: number;
  title: string;
  overview: string;
  poster: string;
  backdrop: string;
  popularity: number;
  release_date: string;
}

const requestNowPlaying = (): AppActions => ({
  type: REQUEST_NOWPLAYING,
  loading: true,
  playing: [],
  error: "",
});

const nowPlayingLoaded = (movies: Movie[]): AppActions => ({
  type: NOWPLAYING_LOADED,
  loading: false,
  playing: movies,
  error: "",
});

const nowPlayingError = (): AppActions => ({
  type: NOWPLAYING_ERROR,
  loading: false,
  playing: [],
  error: "Error loading movies",
});

const requestPopular = (): AppActions => ({
  type: REQUEST_POPULAR,
  loading: true,
  popular: [],
  error: "",
});

const popularLoaded = (movies: Movie[]): AppActions => ({
  type: POPULAR_LOADED,
  loading: false,
  popular: movies,
  error: "",
});

const popularError = (): AppActions => ({
  type: POPULAR_ERROR,
  loading: false,
  popular: [],
  error: "Oops! Something went wrong while fetching Popular Movies",
});

const requestUpcoming = (): AppActions => ({
  type: REQUEST_UPCOMING,
  loading: true,
  upcoming: [],
  error: "",
});

const upcomingLoaded = (movies: Movie[]): AppActions => ({
  type: UPCOMING_LOADED,
  loading: false,
  upcoming: movies,
  error: "",
});

const upcomingError = (): AppActions => ({
  type: UPCOMING_ERROR,
  loading: false,
  upcoming: [],
  error: "Oops! Something went wrong while fetching Upcoming Movies",
});

export const fetchNowPlaying = () => {
  return (dispatch: Dispatch<AppActions>) => {
    let moviesList: Movie[] = [];

    dispatch(requestNowPlaying());
    try {
      axios.get(nowPlayingLink).then((response) => {
        response.data.results.map((res: objectCell) => {
          let movieData: Movie;
          movieData = {
            id: res.id,
            title: res.title,
            overview: res.overview,
            poster: res.poster_path,
            backdrop: res.backdrop_path,
            popularity: res.popularity,
            release_date: res.release_date,
          };
          moviesList.push(movieData);
        });
        dispatch(nowPlayingLoaded(moviesList));
      });
    } catch (e) {
      dispatch(nowPlayingError());
    }
  };
};

export const fetchPopular = () => {
  return async (dispatch: Dispatch<AppActions>) => {
    let moviesList: Movie[] = [];

    dispatch(requestPopular());

    try {
      const request = await axios.get(popularLink);
      const { results } = request.data;

      results.map((result: objectCell) => {
        let movieCell: objectCell;

        movieCell = {
          id: result.id,
          title: result.title,
          overview: result.overview,
          poster: result.poster_path,
          backdrop: result.backdrop_path,
          popularity: result.popularity,
          release_date: result.release_date,
        };

        moviesList.push(movieCell);
      });

      dispatch(popularLoaded(moviesList));
    } catch (error) {
      dispatch(popularError());
    }
  };
};

export const fetchUpcoming = () => {
  return async (dispatch: Dispatch<AppActions>) => {
    let moviesList: Movie[] = [];

    dispatch(requestUpcoming());

    try {
      const request = await axios.get(upcomingLink);

      const { results } = request.data;

      results.map((result: objectCell) => {
        let movieData: Movie;

        movieData = {
          id: result.id,
          title: result.title,
          overview: result.overview,
          backdrop: result.backdrop_path,
          poster: result.poster_path,
          popularity: result.popularity,
          release_date: result.release_date,
        };
        moviesList.push(movieData);
      });

      dispatch(upcomingLoaded(moviesList));
    } catch (error) {
      dispatch(upcomingError());
    }
  };
};
