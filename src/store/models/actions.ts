import { moviesActionsTypes } from "../movies/models/actions.types";

export type AppActions = moviesActionsTypes;
